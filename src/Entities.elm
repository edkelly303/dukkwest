module Entities exposing (downwardsRobin, leftwardsRobin, player, pudding, rightwardsRobin, santa, tree)

import Sprites
import Types exposing (Entity, EntityClass(..), RobinType(..), Vec2)


player : { a | screenWidth : Int } -> Entity
player { screenWidth } =
    { type_ = Player
    , sprite =
        \velocity ->
            if velocity.x < 0 then
                Sprites.playerLeft
            else if velocity.x > 0 then
                Sprites.playerRight
            else
                Sprites.playerDown
    , position = Vec2 400 250
    , velocity = Vec2 0 10
    , minVelocity = Vec2 -10 5
    , maxVelocity = Vec2 10 10
    , acceleration = Vec2 2 1
    }


rightwardsRobin : Entity
rightwardsRobin =
    { type_ = Robin Rightwards
    , sprite = \_ -> Sprites.robinRight
    , position = Vec2 -250 400
    , velocity = Vec2 10 12.5
    , minVelocity = Vec2 -10 10
    , maxVelocity = Vec2 10 10
    , acceleration = Vec2 1 1
    }


leftwardsRobin : Entity
leftwardsRobin =
    { type_ = Robin Leftwards
    , sprite = \_ -> Sprites.robinLeft
    , position = Vec2 1050 400
    , velocity = Vec2 -10 12.5
    , minVelocity = Vec2 -10 10
    , maxVelocity = Vec2 10 10
    , acceleration = Vec2 1 1
    }


downwardsRobin : Entity
downwardsRobin =
    { type_ = Robin Downwards
    , sprite = \_ -> Sprites.robinDown
    , position = Vec2 400 -250
    , velocity = Vec2 0 25
    , minVelocity = Vec2 -2 20
    , maxVelocity = Vec2 2 25
    , acceleration = Vec2 1 1
    }


tree : Entity
tree =
    { type_ = Tree
    , sprite = \_ -> Sprites.tree
    , position = Vec2 500 -100
    , velocity = Vec2 0 0
    , minVelocity = Vec2 0 0
    , maxVelocity = Vec2 0 0
    , acceleration = Vec2 0 0
    }


pudding : Entity
pudding =
    { type_ = Pudding
    , sprite = \_ -> Sprites.pudding
    , position = Vec2 400 -250
    , velocity = Vec2 0 20
    , minVelocity = Vec2 -5 15
    , maxVelocity = Vec2 5 30
    , acceleration = Vec2 3 3
    }


santa : Entity
santa =
    { type_ = Santa
    , sprite = \_ -> Sprites.santa
    , position = Vec2 400 600
    , velocity = Vec2 0 -10
    , minVelocity = Vec2 0 -20
    , maxVelocity = Vec2 0 -10
    , acceleration = Vec2 0 5
    }
