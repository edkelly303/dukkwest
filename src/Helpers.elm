module Helpers exposing (hitBoxCoordinates, scaleNumber, spriteCoordinates)

import Types exposing (Entity, EntityClass(..), Model, RobinType(..), Sprite, Vec2)


type alias Box =
    { left : Float
    , right : Float
    , top : Float
    , bottom : Float
    , width : Float
    , height : Float
    }


hitBoxCoordinates : Entity -> Float -> Box
hitBoxCoordinates entity scale =
    let
        sprite =
            entity.sprite entity.velocity

        hitboxOffsetX =
            scale * sprite.hitboxOffset.x

        hitboxOffsetY =
            scale * sprite.hitboxOffset.y

        hitboxSizeX =
            scale * sprite.hitboxSize.x

        hitboxSizeY =
            scale * sprite.hitboxSize.y
    in
    { left = (entity.position.x + hitboxOffsetX) - (hitboxSizeX / 2)
    , right = (entity.position.x + hitboxOffsetX) + (hitboxSizeX / 2)
    , top = (entity.position.y + hitboxOffsetY) - (hitboxSizeY / 2)
    , bottom = (entity.position.y + hitboxOffsetY) + (hitboxSizeY / 2)
    , width = hitboxSizeX
    , height = hitboxSizeY
    }


spriteCoordinates : Vec2 -> Float -> Sprite -> Box
spriteCoordinates position scale sprite =
    { left = position.x - (scale * sprite.size.x / 2)
    , right = position.x + (scale * sprite.size.x / 2)
    , top = position.y - (scale * sprite.size.y / 2)
    , bottom = position.y + (scale * sprite.size.y / 2)
    , width = scale * sprite.size.x
    , height = scale * sprite.size.y
    }


scaleNumber : Float -> Float -> Float -> Float
scaleNumber minFloat maxFloat float =
    let
        cleanFloat =
            clamp 0 1 float

        cleanMax =
            clamp minFloat maxFloat maxFloat

        range =
            cleanMax - minFloat
    in
    (float * range) + minFloat
